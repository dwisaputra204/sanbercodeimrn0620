import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet, Image} from 'react-native';
import { Ionicons } from '@expo/vector-icons'; 
import data from '../skillData.json';


export default class Skill extends React.Component {
    render(){
        return (
                <View style={styles.container}>
                    <View style={styles.backButton}>
                        <Ionicons name="md-arrow-back" size={35} color="white" />
                    </View>
                    <View style={styles.profile}>
                        <Image source={require("../image/ria.jpg")} style={{width: 70, height: 70, borderRadius: 5}}/>
                        <View style={{marginLeft: 8}}>
                            <Text style={{fontSize: 16, fontWeight: "bold"}}>Ria Anggraeni</Text>
                            <Text style={{fontSize: 14, color : "#acacac"}}>Front-End Developer</Text>
                        </View>
                    </View>
                    <View style={styles.skills}>
                        <Text style={{fontSize : 18, marginBottom: 5}}>Programming Languages</Text>
                        {data.programming.map(item => {
                            return <View style={styles.skillItem}>
                            <Image resizeMode="cover" source={{uri : item.icon}} style={{width: 50, height: 50}} />
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize : 10}}>{item.level}</Text>
                                <Text style={{fontSize : 10, color : "#0B9289"}}>{item.percentage}</Text>
                            </View>
                            </View>
                        })}
                    </View>
                    <View style={{borderWidth: 2, borderColor: "#096D67"}}></View>
                    <View style={styles.skills}>
                        <Text style={{fontSize : 18, marginBottom: 5}}>Framework</Text>
                        {data.framework.map(item => {
                            return <View style={styles.skillItem}>
                                <Image resizeMode="cover" source={{uri : item.icon}} style={{width: 50, height: 50}} />
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize : 10}}>{item.level}</Text>
                                <Text style={{fontSize : 10, color : "#0B9289"}}>{item.percentage}</Text>
                            </View>
                            </View>
                        })}
                    </View>
                    <View style={{borderWidth: 2, borderColor: "#096D67"}}></View>
                    <View style={styles.skills}>
                        <Text style={{fontSize : 18, marginBottom: 5}}>Framework</Text>
                        {data.technology.map(item => {
                            return <View style={styles.skillItem}>
                            <Image resizeMode="cover" source={{uri : item.icon}} style={{width: 50, height: 50}} />
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize : 10}}>{item.level}</Text>
                                <Text style={{fontSize : 10, color : "#0B9289"}}>{item.percentage}</Text>
                            </View>
                            </View>
                        })}
                    </View>
                </View>           
        );
    }
}


const styles = StyleSheet.create({
    container :{
        flex : 1,
        padding: 20
    },
    backButton : {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 30,
        paddingHorizontal: 10,
        width: 40,
        height: 40,
        backgroundColor: "black",
        borderRadius: 20,
    },
    profile : {
        marginTop : 30,
        flexDirection : "row",
        alignItems : "center"
    },
    skills : {
        marginTop : 20
    },
    skillItem : {
        flexDirection : "row",
        alignItems : "center",
        marginVertical : 10
    }
});
