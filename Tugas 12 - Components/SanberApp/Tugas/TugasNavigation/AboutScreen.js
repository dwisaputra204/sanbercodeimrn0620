import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, TextInput} from 'react-native';
import { Ionicons } from '@expo/vector-icons'; 
import { AntDesign } from '@expo/vector-icons'; 

export default class Login extends Component {
    render(){
        return(
            <View style={styles.container}>                
                <View style={styles.profile}>
                    <Image resizeMode="cover" source={require("./images/profile.jpg")} style={{width: 150, height: 150, borderRadius: 100}}/>
                    <Text style={{marginVertical: 10,color:"#0B9289", fontSize: 24,fontWeight: "bold", lineHeight: 29}}>Dwi Saputra</Text>
                </View>
                <View style={styles.socmed}>
                    <View style={styles.socmedItem}>
                        <AntDesign name="instagram" size={35} color="black" />
                        <Text style={{fontSize:14,lineHeight: 17, marginLeft: 10}}>@Dwisaputra204</Text>
                    </View>
                    <View style={styles.socmedItem}>
                        <AntDesign name="facebook-square" size={35} color="black" />
                        <Text style={{fontSize:14,lineHeight: 17, marginLeft: 10}}>@Dwisaputra_gtimbang</Text>
                    </View>
                    <View style={styles.socmedItem}>
                        <AntDesign name="twitter" size={35} color="black" />
                        <Text style={{fontSize:14,lineHeight: 17, marginLeft: 10}}>@Dwisaputrakeren</Text>
                    </View>
                    <View style={{borderWidth: 2, borderColor: "#096D67", marginTop: 29}}></View>
                </View>
                <View style={styles.portfolio}>
                    <View style={styles.portfolioItem}>
                        <AntDesign name="github" size={35} color="black" />
                        <Text style={{fontSize:14,lineHeight: 17, marginLeft: 10}}>github.com/Dwisaputra204/</Text>
                    </View>
                    <View style={styles.portfolioItem}>
                        <AntDesign name="linkedin-square" size={35} color="black" />
                        <Text style={{fontSize:14,lineHeight: 17, marginLeft: 10}}>linkedin.com/in/dwi-saputra/</Text>
                    </View>
                </View>
            </View>
        )}
}


const styles = StyleSheet.create({
    container :{
        flex: 1,
    },
    profile: {
        marginTop : 50,
        flexDirection: "column",
        alignItems: "center",        
    },
    socmed :{
        marginTop: 40,
        paddingHorizontal: 20
    },
    socmedItem: {
        marginTop: 20,
        flexDirection: "row",
        alignItems: "center"
    },
    portfolio :{
        marginTop: 10,
        paddingHorizontal: 20
    },
    portfolioItem: {
        marginTop: 20,
        flexDirection: "row",
        alignItems: "center"
    },
});

