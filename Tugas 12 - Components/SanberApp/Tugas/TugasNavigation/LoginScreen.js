import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, TextInput} from 'react-native';


export default class Login extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image resizeMode="contain" source={require("./images/logo-sanber.png")} style={{width: 300, height: 100}}/>
                </View>
                <Text style={styles.AppName}>SanberApp</Text>
                <View style={styles.form}>
                    <Text style={styles.formDesc}>Username / Email</Text>
                    <TextInput style={styles.formInput}>example@gmail.com</TextInput>
                    <Text style={styles.formDesc}>Password</Text>
                    <TextInput style={styles.formInput}>*********</TextInput>
                    <Text style={styles.formDesc}>Konfirmasi Password</Text>
                    <TextInput style={styles.formInput}>*********</TextInput>
                    <TouchableOpacity>
                        <View style={styles.formButtonContainer}>
                            <View style={styles.formButton}>
                                <Text style={{color : "#9FFFF9", textAlign: "center"}}>Sign Up</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                
            </View>
        )}
}


const styles = StyleSheet.create({
    container :{
        flex: 1,
    },
    logo : {
        flexDirection: 'row',
        justifyContent: "center",
        marginTop: 100,
        marginHorizontal: 100,
    },
    AppName : {
        fontWeight: "bold",
        fontSize: 24, 
        lineHeight: 29, 
        color: "#096D67", 
        textAlign: "center",         
    },
    form : {
        marginTop: 36,
        marginHorizontal: 50,
    },
    formDesc: {
        fontSize:12, 
        lineHeight: 15, 
        color: '#acacac'
    },
    formInput: {
        borderWidth: 1, 
        borderColor: '#096D67', 
        paddingVertical: 5, 
        paddingHorizontal : 10,
        fontSize: 14, 
        lineHeight: 17, 
        marginBottom: 20
    },
    formButtonContainer: {
        flexDirection: "row",
        justifyContent: "center"
    },
    formButton: {
        width: 125,
        height: 50,
        backgroundColor: "#0B9289",
        borderRadius: 5,
        justifyContent: "center",
    }
});

