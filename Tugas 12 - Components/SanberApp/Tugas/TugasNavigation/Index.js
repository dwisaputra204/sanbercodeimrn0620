import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from "@react-navigation/stack";
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import SkillScreen from './SkillScreen';
import AddScreen from './AddScreen';
import ProjectScreen from './ProjectScreen';

const AuthStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const TabsScreen = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Skills" component={SkillScreen}/>
            <Tab.Screen name="Project" component={ProjectScreen}/>
            <Tab.Screen name="AddScreen" component={AddScreen}/>
            
        </Tab.Navigator>
    )
}

const App = () => {
    return(
        <NavigationContainer>             
            <Drawer.Navigator>
                <Drawer.Screen name="Home" component={LoginScreen} />
                <Drawer.Screen name="Tab" component={TabsScreen} />
                <Drawer.Screen name="About" component={AboutScreen} />
            </Drawer.Navigator>
        </NavigationContainer>
    )
}

export default App;