import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import LoginScreen from './LoginScreen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Home = ({navigation}) => (
  <ScreenContainer>
    <Text>Master List Screen</Text>
    <Button title="Login" onPress={() => navigation.push('Login') }/>
    <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
  </ScreenContainer>
);

export const Login = () => (
    LoginScreen
);


export const Details = () => (
    <ScreenContainer>
      <Text>Details Screen</Text>  
    </ScreenContainer>
  );