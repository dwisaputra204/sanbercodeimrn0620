import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import YoutubeUI from './Tugas/Tugas12/App';
import Login from './Tugas/Tugas13/LoginScreen';
import About from './Tugas/Tugas13/AboutScreen';
import Tugas14 from './Tugas/Tugas14/App';
import Tugas15 from './Tugas/Tugas15/index';
import TugasNavigation from './Tugas/TugasNavigation/Index';
import Quiz3 from './Tugas/Quiz3/index';
export default function App() {
  return (
    // <YoutubeUI />
    // <About />
    // <Login/>
    // <Tugas15 />
    // <TugasNavigation/>
    <Quiz3/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
