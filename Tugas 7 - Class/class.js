// NO 1
class Animal {
    constructor (name, legs=4, cold_blooded=false){
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
}

// ======== Release 0 =======

var sheep = new Animal("shaun");
console.log(`"${sheep.name}"`);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

// ======== Release 1 =======

class Ape extends Animal {
    constructor (name){
        super(name);
        this.legs = 2;
    }
    yell (){
        console.log("auooo");
    }
}

class Frog extends Animal {
    constructor (name){
        super(name);
        this.cold_blooded = true;
    }
    jump (){
        console.log("hop hop");
    }
}

var sungokong = new Ape("Kera Sakti");
var kodok = new Frog("buduk");

sungokong.yell();
kodok.jump();


// NO 2
class Clock {
    constructor ({template}){
        this.template = template;
    }
    render(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop (){
        clearInterval(this.timer);
    }
  
    start (){
      this.render();
      this.timer = setInterval(this.render.bind(this), 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
