// NO 1
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
var now = new Date();
var thisYear = now.getFullYear();

function arrayToObject(data){
    var obj = {};
    if(data.length == 0){
        console.log('""');
    }else {
        for(let i = 0; i < data.length; i++){
            var tempAge;
            tempAge = thisYear - data[i][3];
            obj = {
                firstName : data[i][0],
                lastName : data[i][1],
                gender : data[i][2],
                age : tempAge
            }
            if(obj.age <= 0 || !obj.age){
                obj.age = "Invalid Birth Year";
            }
            console.log(`${i+1}. ${obj.firstName} ${obj.lastName} : {
                firstName : "${obj.firstName}"
                lastName : "${obj.lastName}"
                gender : "${obj.gender}"
                age : ${obj.age}
            }`);
        }
    }  
    
}
arrayToObject(people);



// NO 2
function shoppingTime(memberId, money){
    var listBarang = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Zoro", 500000],
        ["Baju H&N", 250000],
        ["Sweater Uniklooh", 175000],
        ["casing", 50000]
    ]

    var barang = [];
    var tempMoney = money;
    if (memberId === "" || money == null){
        return "Mohon maaf, Toko X hanya berlaku untuk member saja";
    }
    if (money < 50000){
        return "Mohon maaf, uang tidak cukup";
    } else {   
        for(let i = 0; i < listBarang.length; i++){
            if(tempMoney >= listBarang[i][1]){
                barang.push(listBarang[i][0]);
                tempMoney -= listBarang[i][1];         
            }
        }
    }
    var obj = {
        memberId : memberId,
        money : money,
        listPurchased : barang,
        changeMoney : tempMoney
    }

    return obj;

}

console.log(shoppingTime('324193hDew2',700000));


// NO 3
function naikAngkot(listPenumpang){
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];   
    var arr = [];
    if (listPenumpang == []){
        return "[]";
    } 
    for (let i = 0; i < listPenumpang.length; i++){
        var bayar = (rute.indexOf(listPenumpang[i][2]) - rute.indexOf(listPenumpang[i][1])) * 2000;
        arr[i] = {
            penumpang : listPenumpang[i][0],
            naikDari : listPenumpang[i][1],
            tujuan : listPenumpang[i][2],
            bayar : bayar
        }
    }
    
    return arr;
}


console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));





























