// No 1
var numbers = [];
function range(startNum = -1, finishNum = -1){
    if (startNum === -1 || finishNum === -1){
        numbers.push(-1);
    } else if(startNum < finishNum){
        for(let i = startNum; i <= finishNum; i++){
            numbers.push(i);
        }
    } else {
        for(let i = startNum; i >= finishNum;i--){
            numbers.push(i);
        }
    }
    return numbers;
}
console.log("---- NO 1 ----");
console.log(range(1,10));

console.log(" ");
// No 2
var numbers2 = [];
function rangeWithStep(startNum = -1, finishNum = -1, step){
    if(startNum < finishNum){
        for(let i = startNum; i <= finishNum; i+= step){
            numbers2.push(i);
        }
    } else {
        for(let i = startNum; i >= finishNum; i-= step){
            numbers2.push(i);
        }
    }
    return numbers2;
}
console.log("---- NO 2 ----");
console.log(rangeWithStep(29,2,4));
console.log("");

// No 3
var numbers3 = [];
var result = 0;
function sum(startNum = 0, finishNum = 0, step = 1){
    if(startNum === 0 && finishNum === 0){
        return 0;
    } else if(startNum < finishNum){
        for(let i = startNum; i <= finishNum; i+= step){
            numbers3.push(i);
        }
    } else {
        for(let i = startNum; i >= finishNum; i-= step){
            numbers3.push(i);
        }
    }

    for(let j = 0; j < numbers3.length; j++){
        result += numbers3[j];
    }

    return result;
}
console.log("---- NO 3 ----");
console.log(sum(1,10));
console.log("");

// NO 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
function dataHandling(input){
    for(let i = 0; i < input.length; i++){
        console.log(`Nomor ID : ${input[i][0]}`);
        console.log(`Nama Lengkap : ${input[i][1]}`);
        console.log(`TTL : ${input[i][2]} ${input[i][3]}`);
        console.log(`Hobi : ${input[i][4]}`);
        console.log("");
    }
}
console.log("---- NO 4 ----");
dataHandling(input);
console.log("");

// NO 5
var temp = [];
var result = "";
function balikKata(kata){
    for(let i = 0; i < kata.length; i++){
        temp.push(kata[i]);
    }
    for(let j = temp.length - 1; j >= 0; j--){
        result += temp[j];
    }
    return result;
}

console.log("---- NO 5 ----");
console.log(balikKata("Dwi Saputra"));
console.log("");

// // NO 6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];


function dataHandling2(input){
    input.splice(1,1,`${input[1]} Elsharawy`);
    input.splice(2,1,`Provinsi ${input[2]}`);
    input.splice(4,1,"Pria");
    input.splice(5,0,"SMA Internasional Metro");
    console.log(input);

    var tempTgl = input[3];
    var tgl = tempTgl.split("/");
    var formatTgl = tempTgl.split("/");
    switch(tgl[1]){
        case "01": { console.log('Januari'); break; }
        case "02": { console.log('Februari'); break; }
        case "03": { console.log('Maret'); break; }
        case "04": { console.log('April'); break; }
        case "05": { console.log('Mei'); break; }
        case "06": { console.log('Juni'); break; }
        case "07": { console.log('Juli'); break; }
        case "08": { console.log('Agustus'); break; }
        case "09": { console.log('September'); break; }
        case "10": { console.log('Oktober'); break; }
        case "11": { console.log('November'); break; }
        case "12": { console.log('Desember'); break; }
    }
    
    var tglSort = tgl.sort(function(value1,value2){
        return value2 - value1;
    });
    
    console.log(tglSort);

    var tglJoin = formatTgl.join("-");
    console.log(tglJoin);

    var tempNama = input[1];
    nama = tempNama.slice(0,14);
    console.log(nama);
    
}

console.log("---- NO 6 ----");
dataHandling2(input);
console.log("");
































