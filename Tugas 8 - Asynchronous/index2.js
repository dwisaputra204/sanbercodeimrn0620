var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var awal = 10000;
function antrian(x){
    if(x == books.length){
        return 0;
    }
    readBooksPromise(awal,books[x]).then(function(fulfilled){
        awal = fulfilled;
        antrian(x+1);
    }).catch(function(error){
        console.log(error);
    })
}

antrian(0);
