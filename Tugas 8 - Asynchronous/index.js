var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var awal = 10000;
function antrian(x){
    if(x == books.length){
        return 0;
    }
    readBooks(awal,books[x], function(callback){
        awal = callback;
        antrian(x+1);
    })
}
antrian(0);
